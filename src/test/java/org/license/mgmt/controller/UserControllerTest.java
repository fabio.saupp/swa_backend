package org.license.mgmt.controller;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.license.mgmt.dto.LoginDto;
import org.license.mgmt.dto.UserDto;
import org.license.mgmt.entity.User;
import org.license.mgmt.exception.UserNotFoundException;
import org.license.mgmt.service.ContractService;
import org.license.mgmt.service.UserService;
import org.mockito.Mockito;

import io.quarkus.security.identity.SecurityIdentity;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.security.TestSecurity;

@QuarkusTest
class UserControllerTest {
	@InjectMock
	private UserService userService;
	@InjectMock
	private ContractService contractService;

	@Inject
	UserController userController;

	@InjectMock
	SecurityIdentity identity;

	@BeforeEach
	public void setup() throws UserNotFoundException {
		Mockito.when(identity.hasRole("ADMIN")).thenReturn(true);
		Mockito.when(userService.getAllUsersByCustomerId(any(Long.class))).thenReturn(getUserList());
		
	}

	@Test
	@TestSecurity(authorizationEnabled = false)
	public void testAllUsersEndpoint() throws Exception {
		given().when().contentType(MediaType.APPLICATION_JSON).get("/v1/users").then().statusCode(200)
				.body(notNullValue());
	}

	@Test
	@TestSecurity(authorizationEnabled = false)
	public void testAllUsersWithContractEndpoint() throws Exception {
		given().when().contentType(MediaType.APPLICATION_JSON).get("/v1/users/bycontract/allusers").then()
				.statusCode(200).body(notNullValue());
	}

	@Test
	@TestSecurity(authorizationEnabled = false)
	public void testAllUsersWithCustomersEndpoint() throws Exception {

		given().when().contentType(MediaType.APPLICATION_JSON).get("/v1/users/customers/1").then().statusCode(200)
				.body(notNullValue());
	}
	
	@Test
	@TestSecurity(authorizationEnabled = false)
	public void testUsersWithLoginEndpoint() throws Exception {
		Mockito.when(userService.loginUser(any(LoginDto.class))).thenReturn(getUserDto());
		given().when().contentType(MediaType.APPLICATION_JSON).body(getLoginDto()).post("/v1/users/login").then().statusCode(200)
				.body(containsString("test"));
	}
	
	@Test
	@TestSecurity(authorizationEnabled = false)
	public void testCreateUsersEndpoint() throws Exception {
		Mockito.when(userService.saveUser(any(UserDto.class))).thenReturn(getUser());
		given().when().contentType(MediaType.APPLICATION_JSON).body(getUserDto()).post("/v1/users").then().statusCode(200)
				.body(containsString("test"));
	}
	
	@Test
	@TestSecurity(authorizationEnabled = false)
	public void testUpdateUsersEndpoint() throws Exception {
		Mockito.when(userService.updateUser(any(Long.class), any(UserDto.class))).thenReturn(getUser());
		given().when().contentType(MediaType.APPLICATION_JSON).body(getUserDto()).put("/v1/users/1").then().statusCode(200)
				.body(containsString("test"));
	}
	
	@Test
	@TestSecurity(authorizationEnabled = false)
	public void testDeleteUsersEndpoint() throws Exception {
		doNothing().when(userService).deleteUser(any(Long.class));
		given().when().contentType(MediaType.APPLICATION_JSON).body(getUserDto()).delete("/v1/users/1").then().statusCode(204);
	}

	private User getUser() {
		User user = new User();
		user.setEmail("test");
		user.setFirstName("test");
		user.setLastName("test");
		user.setId(1l);
		user.setMobile("test");
		user.setPassword("test");
		user.setPhone("test");
		user.setRole("ADMIN");
		user.setUsername("test");
		return user;
	}

	private List<User> getUserList() {
		List<User> users = new ArrayList<>();
		users.add(getUser());
		return users;
	}
	
	private LoginDto getLoginDto() {
		LoginDto dto = new LoginDto();
		dto.setUsername("test");
		dto.setPassword("test");
		return dto;
	}
	
	private UserDto getUserDto() {
		UserDto dto = new UserDto();
		dto.setEmail("test");
		dto.setFirstName("test");
		dto.setLastName("test");
		dto.setId(1l);
		dto.setMobile("test");
		dto.setPassword("test");
		dto.setPhone("test");
		dto.setRole("ADMIN");
		dto.setUsername("test");
		return dto;
	}
}
