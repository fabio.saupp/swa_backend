package org.license.mgmt.controller;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.license.mgmt.dto.ContractDataDto;
import org.license.mgmt.dto.ContractDto;
import org.license.mgmt.entity.Contract;
import org.license.mgmt.entity.Customer;
import org.license.mgmt.entity.User;
import org.license.mgmt.exception.UserNotFoundException;
import org.license.mgmt.service.ContractService;
import org.license.mgmt.service.CustomerService;
import org.license.mgmt.service.UserService;
import org.mockito.Mockito;

import io.quarkus.security.identity.SecurityIdentity;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.security.TestSecurity;

@QuarkusTest
class ContractControllerTest {
	
	@InjectMock
	private ContractService contractService;
	
	@InjectMock
	private CustomerService customerService;
	
	@InjectMock
	private UserService userService;
	
	@Inject
	UserController userController;

	@InjectMock
	SecurityIdentity identity;

	@BeforeEach
	public void setup() throws UserNotFoundException {
		Mockito.when(identity.hasRole("ADMIN")).thenReturn(true);
	}

	@Test
	@TestSecurity(authorizationEnabled = false)
	public void testAllUsersEndpoint() throws Exception {
		Mockito.when(contractService.getAllContracts()).thenReturn(getContractDataDtoList());
		given().when().contentType(MediaType.APPLICATION_JSON).get("/v1/contracts").then().statusCode(200)
				.body(notNullValue());
	}
	
	@Test
	@TestSecurity(authorizationEnabled = false)
	public void testGetAllContractsByCustomerIdEndpoint() throws Exception {
		Mockito.when(contractService.getAllContractsByCustomerId(any(Long.class))).thenReturn(getContractList());
		given().when().contentType(MediaType.APPLICATION_JSON).get("/v1/contracts/customers/1").then().statusCode(200)
				.body(notNullValue());
	}
	
	@Test
	@TestSecurity(authorizationEnabled = false)
	public void testGetCustomerByUserIdEndpoint() throws Exception {
		Mockito.when(contractService.getAllContractsByUserId(any(Long.class))).thenReturn(getContractList());
		given().when().contentType(MediaType.APPLICATION_JSON).get("/v1/contracts/user/1").then().statusCode(200)
				.body(notNullValue());
	}
	
	@Test
	@TestSecurity(authorizationEnabled = false)
	public void testCreateContractEndpoint() throws Exception {
		Mockito.when(contractService.getAllContractsByUserId(any(Long.class))).thenReturn(getContractList());
		Mockito.when(contractService.saveContract(any(Contract.class))).thenReturn(getContract());
		Mockito.when(userService.findByUserName(any(String.class))).thenReturn(getUser());
		Mockito.doNothing().when(contractService).saveContractUserMap(any(Contract.class), any(User.class));
		Mockito.when(customerService.getCustomerById(any(Long.class))).thenReturn(getCustomer());
		given().when().contentType(MediaType.APPLICATION_JSON).body(getContractDto()).post("/v1/contracts").then().statusCode(200)
				.body(notNullValue());
	}
	
	@Test
	@TestSecurity(authorizationEnabled = false)
	public void testUpdateContractEndpoint() throws Exception {
		Mockito.when(contractService.updateContract(any(Long.class), any(ContractDto.class))).thenReturn(getContract());
		Mockito.when(contractService.saveContract(any(Contract.class))).thenReturn(getContract());
		Mockito.when(userService.findByUserName(any(String.class))).thenReturn(getUser());
		Mockito.doNothing().when(contractService).saveContractUserMap(any(Contract.class), any(User.class));
		Mockito.when(customerService.getCustomerById(any(Long.class))).thenReturn(getCustomer());
		given().when().contentType(MediaType.APPLICATION_JSON).body(getContractDto()).put("/v1/contracts/1").then().statusCode(200)
				.body(notNullValue());
	}
	
	@Test
	@TestSecurity(authorizationEnabled = false)
	public void testDeleteContractEndpoint() throws Exception {
		Mockito.when(contractService.updateContract(any(Long.class), any(ContractDto.class))).thenReturn(getContract());
		Mockito.when(contractService.saveContract(any(Contract.class))).thenReturn(getContract());
		Mockito.when(userService.findByUserName(any(String.class))).thenReturn(getUser());
		Mockito.doNothing().when(contractService).deleteContract(any(Long.class));
		Mockito.when(customerService.getCustomerById(any(Long.class))).thenReturn(getCustomer());
		given().when().contentType(MediaType.APPLICATION_JSON).body(getContractDto()).delete("/v1/contracts/1").then().statusCode(204)
				.body(notNullValue());
	}
	
	private Contract getContract() {
		Contract cus = new Contract();
		cus.setCustomer(getCustomer());
		cus.setEndDate(LocalDate.now());
		cus.setFeature1(1);
		cus.setFeature2(1);
		cus.setFeature3(1);
		cus.setIp1("1");
		cus.setIp2("1");
		cus.setIp3("1");
		cus.setLicenseKey("test");
		cus.setVersion("test");
		cus.setStartDate(LocalDate.now());
		cus.setId(1l);
		return cus;
	}
	
	private ContractDto getContractDto() {
		ContractDto cus = new ContractDto();
		cus.setCustomerId(1);
		//cus.setStartDate(LocalDate.now());
		//cus.setEndDate(LocalDate.now());
		cus.setFeature1(1);
		cus.setFeature2(1);
		cus.setFeature3(1);
		cus.setIp1("1");
		cus.setIp2("1");
		cus.setIp3("1");
		cus.setLicenseKey("test");
		cus.setVersion("test");
		List<String> list = new ArrayList<>();
		list.add("test");
		cus.setUsers(list);
		return cus;
	}

	private List<Contract> getContractList() {
		List<Contract> list = new ArrayList<>();
		list.add(getContract());
		return list;
	}

	private User getUser() {
		User user = new User();
		user.setEmail("test");
		user.setFirstName("test");
		user.setLastName("test");
		user.setId(1l);
		user.setMobile("test");
		user.setPassword("test");
		user.setPhone("test");
		user.setRole("ADMIN");
		user.setUsername("test");
		return user;
	}
	
	public ContractDataDto getContractDataDto() {
		ContractDataDto dto = new ContractDataDto(1l,LocalDate.now(), getCustomer(), getUserList(), LocalDate.now(), "1","1","1","test", 1,1,1,"test" );
		return dto;
	}
	
	public List<ContractDataDto> getContractDataDtoList() {
		List<ContractDataDto> list = new ArrayList<>();
		list.add(getContractDataDto());
		return list;
	}

	private List<User> getUserList() {
		List<User> users = new ArrayList<>();
		users.add(getUser());
		return users;
	}


	private Customer getCustomer() {
		Customer cus = new Customer();
		cus.setAddress("test");
		cus.setDepartment("test");
		cus.setName("test");
		cus.setId(1l);
		return cus;
	}
}
