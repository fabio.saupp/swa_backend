package org.license.mgmt.controller;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.license.mgmt.dto.ContractDataDto;
import org.license.mgmt.dto.CustomerDto;
import org.license.mgmt.entity.Contract;
import org.license.mgmt.entity.Customer;
import org.license.mgmt.entity.User;
import org.license.mgmt.exception.UserNotFoundException;
import org.license.mgmt.service.CustomerService;
import org.mockito.Mockito;

import io.quarkus.security.identity.SecurityIdentity;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.security.TestSecurity;

@QuarkusTest
class CustomerControllerTest {
	
	@InjectMock
	private CustomerService customerService;

	@Inject
	CustomerController customController;

	@InjectMock
	SecurityIdentity identity;

	@BeforeEach
	public void setup() throws UserNotFoundException {
		Mockito.when(identity.hasRole("ADMIN")).thenReturn(true);
	}

	@Test
	@TestSecurity(authorizationEnabled = false)
	public void testGetAllCustomersEndpoint() throws Exception {
		Mockito.when(customerService.getAllCustomers(any(String.class))).thenReturn(getCustomerList());
		given().when().contentType(MediaType.APPLICATION_JSON).get("/v1/customers?name=test").then().statusCode(200)
				.body(notNullValue());
	}
	
	@Test
	@TestSecurity(authorizationEnabled = false)
	public void testGetCustomersByIdEndpoint() throws Exception {
		Mockito.when(customerService.getCustomerById(any(Long.class))).thenReturn(getCustomer());
		given().when().contentType(MediaType.APPLICATION_JSON).get("/v1/customers/1").then().statusCode(200)
				.body(notNullValue());
	}
	
	@Test
	@TestSecurity(authorizationEnabled = false)
	public void testGetAllCustomersByUserIdEndpoint() throws Exception {
		Mockito.when(customerService.getAllCustomerByUserId(any(Long.class))).thenReturn(getCustomer());
		given().when().contentType(MediaType.APPLICATION_JSON).get("/v1/customers/1").then().statusCode(204)
				.body(notNullValue());
	}
	
	
	@Test
	@TestSecurity(authorizationEnabled = false)
	public void testSaveCustomerEndpoint() throws Exception {
		Mockito.when(customerService.saveCustomer(any(Customer.class))).thenReturn(getCustomer());
		given().when().body(getCustomerDto()).contentType(MediaType.APPLICATION_JSON).post("/v1/customers").then().statusCode(200)
				.body(notNullValue());
	}
	
	@Test
	@TestSecurity(authorizationEnabled = false)
	public void testUpdateCustomerEndpoint() throws Exception {
		Mockito.when(customerService.updateCustomer(any(Long.class), any(Customer.class))).thenReturn(getCustomer());
		given().when().body(getCustomerDto()).contentType(MediaType.APPLICATION_JSON).put("/v1/customers/1").then().statusCode(200)
				.body(notNullValue());
	}
	
	@Test
	@TestSecurity(authorizationEnabled = false)
	public void testDeleteCustomerEndpoint() throws Exception {
		Mockito.doNothing().when(customerService).deleteCustomer(any(Long.class));
		given().when().body(getCustomerDto()).contentType(MediaType.APPLICATION_JSON).delete("/v1/customers/1").then().statusCode(204)
				.body(notNullValue());
	}
	
	private Contract getContract() {
		Contract cus = new Contract();
		cus.setCustomer(getCustomer());
		//cus.setEndDate(LocalDate.now());
		cus.setFeature1(1);
		cus.setFeature2(1);
		cus.setFeature3(1);
		cus.setIp1("1");
		cus.setIp2("1");
		cus.setIp3("1");
		cus.setLicenseKey("test");
		cus.setVersion("test");
		//cus.setStartDate(LocalDate.now());
		cus.setId(1l);
		return cus;
	}
	
	private CustomerDto getCustomerDto() {
		CustomerDto cus = new CustomerDto();
		cus.setAddress("test");
		cus.setDepartment("test");
		cus.setName("test");
		cus.setContracts(getContractList());
		return cus;
	}

	private List<Contract> getContractList() {
		List<Contract> list = new ArrayList<>();
		list.add(getContract());
		return list;
	}

	private User getUser() {
		User user = new User();
		user.setEmail("test");
		user.setFirstName("test");
		user.setLastName("test");
		user.setId(1l);
		user.setMobile("test");
		user.setPassword("test");
		user.setPhone("test");
		user.setRole("ADMIN");
		user.setUsername("test");
		return user;
	}
	
	public ContractDataDto getContractDataDto() {
		ContractDataDto dto = new ContractDataDto(1l,LocalDate.now(), getCustomer(), getUserList(), LocalDate.now(), "1","1","1","test", 1,1,1,"test" );
		return dto;
	}
	
	public List<ContractDataDto> getContractDataDtoList() {
		List<ContractDataDto> list = new ArrayList<>();
		list.add(getContractDataDto());
		return list;
	}

	private List<User> getUserList() {
		List<User> users = new ArrayList<>();
		users.add(getUser());
		return users;
	}

	private Customer getCustomer() {
		Customer cus = new Customer();
		cus.setAddress("test");
		cus.setDepartment("test");
		cus.setName("test");
		cus.setId(1l);
		return cus;
	}

	private List<Customer> getCustomerList() {
		List<Customer> list = new ArrayList<>();
		list.add(getCustomer());
		return list;
	}
	
}
