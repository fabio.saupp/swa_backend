package org.license.mgmt.exception;

public class ContractNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public ContractNotFoundException(String message) {
        super(message);
    }
}
