package org.license.mgmt.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "contract")
public class Contract implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne
	@JoinColumn(name = "CUSTOMER_ID")
	private Customer customer;

	private LocalDate startDate;

	private LocalDate endDate;

	private String ip1;

	private String ip2;

	private String ip3;

	private String version;

	private double feature1;

	private double feature2;

	private double feature3;

	private String licenseKey;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public String getIp1() {
		return ip1;
	}

	public void setIp1(String ip1) {
		this.ip1 = ip1;
	}

	public String getIp2() {
		return ip2;
	}

	public void setIp2(String ip2) {
		this.ip2 = ip2;
	}

	public String getIp3() {
		return ip3;
	}

	public void setIp3(String ip3) {
		this.ip3 = ip3;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public double getFeature1() {
		return feature1;
	}

	public void setFeature1(double feature1) {
		this.feature1 = feature1;
	}

	public double getFeature2() {
		return feature2;
	}

	public void setFeature2(double feature2) {
		this.feature2 = feature2;
	}

	public double getFeature3() {
		return feature3;
	}

	public void setFeature3(double feature3) {
		this.feature3 = feature3;
	}

	public String getLicenseKey() {
		return licenseKey;
	}

	public void setLicenseKey(String licenseKey) {
		this.licenseKey = licenseKey;
	}

	@Override
	public String toString() {
		return "Contract [id=" + id + ", customer=" + customer + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", ip1=" + ip1 + ", ip2=" + ip2 + ", ip3=" + ip3 + ", version=" + version + ", feature1=" + feature1
				+ ", feature2=" + feature2 + ", feature3=" + feature3 + ", licenseKey=" + licenseKey + "]";
	}

}
