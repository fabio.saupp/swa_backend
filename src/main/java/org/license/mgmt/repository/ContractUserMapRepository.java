package org.license.mgmt.repository;

import javax.enterprise.context.ApplicationScoped;

import org.license.mgmt.entity.ContractUserMap;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class ContractUserMapRepository implements PanacheRepository<ContractUserMap> {

}
