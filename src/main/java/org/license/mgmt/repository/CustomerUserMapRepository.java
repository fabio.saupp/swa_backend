package org.license.mgmt.repository;

import javax.enterprise.context.ApplicationScoped;

import org.license.mgmt.entity.CustomerUserMap;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class CustomerUserMapRepository implements PanacheRepository<CustomerUserMap> {

}

