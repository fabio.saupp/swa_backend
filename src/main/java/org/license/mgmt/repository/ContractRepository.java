package org.license.mgmt.repository;

import javax.enterprise.context.ApplicationScoped;

import org.license.mgmt.entity.Contract;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class ContractRepository implements PanacheRepository<Contract> {
}
