package org.license.mgmt.repository;

import javax.enterprise.context.ApplicationScoped;

import org.license.mgmt.entity.Customer;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class CustomerRepository implements PanacheRepository<Customer> {
}