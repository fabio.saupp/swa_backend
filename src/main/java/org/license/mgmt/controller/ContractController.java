package org.license.mgmt.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.license.mgmt.dto.ContractDataDto;
import org.license.mgmt.dto.ContractDto;
import org.license.mgmt.entity.Contract;
import org.license.mgmt.entity.Customer;
import org.license.mgmt.entity.User;
import org.license.mgmt.exception.ContractNotFoundException;
import org.license.mgmt.exception.CustomerNotFoundException;
import org.license.mgmt.exception.UserNotFoundException;
import org.license.mgmt.exception.handler.ExceptionHandler;
import org.license.mgmt.service.ContractService;
import org.license.mgmt.service.CustomerService;
import org.license.mgmt.service.UserService;

@RequestScoped
@Path("/v1/contracts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@SecurityScheme(securitySchemeName = "Basic Auth", type = SecuritySchemeType.HTTP, scheme = "basic")
public class ContractController {
	
	private final ContractService contractService;
	
	private final CustomerService customerService;
	
	private final UserService userService;

    @Inject
    public ContractController(ContractService contractService, CustomerService customerService,UserService userService) {
        this.contractService = contractService;
        this.customerService = customerService;
        this.userService = userService;
    }
    
    @GET
    @RolesAllowed({"USER", "ADMIN"})
    @Operation(summary = "Gets contracts", description = "Lists all available contracts")
    @APIResponses(value = @APIResponse(responseCode = "200", description = "Success",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Contract.class))))
    public Map<Customer,List<ContractDataDto>> getContracts() {
    	
    	Map<Customer,List<ContractDataDto>> map = contractService.getAllContracts().stream()
    			.collect(Collectors.groupingBy(ContractDataDto::getCustomer,LinkedHashMap::new, Collectors.toList()));
        return map;
    }
    
    @GET
    @RolesAllowed({"USER", "ADMIN"})
    @Path("/customers/{customerId}")
    @Operation(summary = "Gets all users by customer id", description = "Lists all available users by customer id")
    @APIResponses(value = @APIResponse(responseCode = "200", description = "Success",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Contract.class))))
    public List<Contract> getUsersByCustomerId(@PathParam("customerId") int customerId) {
        return contractService.getAllContractsByCustomerId(customerId);
    }
    
    @GET
    @RolesAllowed({"USER", "ADMIN"})
    @Path("/user/{userId}")
    @Operation(summary = "Gets all users by customer id", description = "Lists all available users by customer id")
    @APIResponses(value = @APIResponse(responseCode = "200", description = "Success",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Contract.class))))
    public List<Contract> getCustomerByUserId(@PathParam("userId") int userId) {
        return contractService.getAllContractsByUserId(userId);
    }

    @GET
    @RolesAllowed({"USER", "ADMIN"})
    @Path("/{id}")
    @Operation(summary = "Gets a contract", description = "Retrieves a contract by id")
    @APIResponses(value = {
            @APIResponse(responseCode = "200", description = "Success",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Contract.class))),
            @APIResponse(responseCode = "404", description="Contract not found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionHandler.ErrorResponseBody.class)))
    })
    public ContractDataDto getContract(@PathParam("id") int id) throws ContractNotFoundException {
    	
    	Contract each = contractService.getContractById(id);
    	return new ContractDataDto(each.getId(), each.getStartDate(), each.getCustomer()
    			, contractService.getUserContractMapCount(each.getId()), each.getEndDate(), each.getIp1(), each.getIp2(), each.getIp3(), 
    			each.getVersion(), each.getFeature1(), each.getFeature2(), each.getFeature3(), each.getLicenseKey());
    	
    }

    @POST
    @RolesAllowed("ADMIN")
    @Operation(summary = "Adds a contract", description = "Creates a contract and persists into database")
    @APIResponses(value = @APIResponse(responseCode = "200", description = "Success",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Contract.class))))
    public Contract createContract(@Valid ContractDto contractDto) throws CustomerNotFoundException, UserNotFoundException {
    	
    	Customer customer = customerService.getCustomerById(contractDto.getCustomerId());
    	Contract contractor = contractDto.toContract();
    	contractor.setCustomer(customer);
    	List<User> users = contractDto.getUsers().stream().map(each -> userService.findByUserName(each)).collect(Collectors.toList());
    	Contract contract = contractService.saveContract(contractor);
    	
    	for(User each : users) {
    		contractService.saveContractUserMap(contract, each);
    	}
    	
        return contract;
    }

    @PUT
    @RolesAllowed({"USER", "ADMIN"})
    @Path("/{id}")
    @Operation(summary = "Updates a contract", description = "Updates an existing contract by id")
    @APIResponses(value = {
            @APIResponse(responseCode = "200", description = "Success",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Contract.class))),
            @APIResponse(responseCode = "404", description="Contract not found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionHandler.ErrorResponseBody.class)))
    })
    public Contract updateContract(@PathParam("id") int id, @Valid ContractDto contractDto) throws ContractNotFoundException {
        return contractService.updateContract(id, contractDto);
    }

    @DELETE
    @RolesAllowed("ADMIN")
    @Path("/{id}")
    @Operation(summary = "Deletes a contract", description = "Deletes a contract by id")
    @APIResponses(value = {
            @APIResponse(responseCode = "204", description = "Success"),
            @APIResponse(responseCode = "404", description="Contract not found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionHandler.ErrorResponseBody.class)))
    })
    public Response deleteContract(@PathParam("id") int id) throws ContractNotFoundException {
    	contractService.deleteContract(id);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

}
