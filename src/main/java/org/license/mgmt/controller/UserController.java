package org.license.mgmt.controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.license.mgmt.dto.LoginDto;
import org.license.mgmt.dto.UserDto;
import org.license.mgmt.entity.Contract;
import org.license.mgmt.entity.Customer;
import org.license.mgmt.entity.User;
import org.license.mgmt.exception.UserNotFoundException;
import org.license.mgmt.exception.handler.ExceptionHandler;
import org.license.mgmt.service.ContractService;
import org.license.mgmt.service.UserService;

@RequestScoped
@Path("/v1/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@SecurityScheme(securitySchemeName = "Basic Auth", type = SecuritySchemeType.HTTP, scheme = "basic")
public class UserController {

	private final UserService userService;

	private final ContractService contractService;

	@Inject
	public UserController(ContractService contractService, UserService userService) {
		this.userService = userService;
		this.contractService = contractService;
	}

	@GET
	@RolesAllowed({ "USER", "ADMIN" })
	@Operation(summary = "Gets users", description = "Lists all available users")
	@APIResponses(value = @APIResponse(responseCode = "200", description = "Success", content = @Content(mediaType = "application/json", schema = @Schema(implementation = User.class))))
	public List<User> getUsers() {
		return userService.getAllUsers();
	}

	@GET
	@RolesAllowed({ "USER", "ADMIN" })
	@Path("/bycontract/allusers")
	@Operation(summary = "Gets contracts", description = "Lists all available contracts")
	@APIResponses(value = @APIResponse(responseCode = "200", description = "Success", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Contract.class))))
	public Map<String, List<User>> getContracts() {

		List<Customer> allCustomers = userService.getAllCustomers();
		return allCustomers.stream().collect(
				Collectors.toMap(Customer::getName, each -> userService.getAllUsersByCustomerId(each.getId())));
	}

	@GET
	@RolesAllowed({ "USER", "ADMIN" })
	@Path("/customers/{customerId}")
	@Operation(summary = "Gets all users by customer id", description = "Lists all available users by customer id")
	@APIResponses(value = @APIResponse(responseCode = "200", description = "Success", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Contract.class))))
	public List<User> getUsersByCustomerId(@PathParam("customerId") int customerId) {
		return userService.getAllUsersByCustomerId(customerId);
	}

	@GET
	@RolesAllowed({ "USER", "ADMIN" })
	@Path("/{id}")
	@Operation(summary = "Gets a user", description = "Retrieves a user by id")
	@APIResponses(value = {
			@APIResponse(responseCode = "200", description = "Success", content = @Content(mediaType = "application/json", schema = @Schema(implementation = User.class))),
			@APIResponse(responseCode = "404", description = "User not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionHandler.ErrorResponseBody.class))) })
	public UserDto getUser(@PathParam("id") int id) throws UserNotFoundException {
		return userService.getUserDtoById(id);
	}

	@POST
	@PermitAll
	@Path("/login")
	@Operation(summary = "Login a user", description = "Login a user by check the username & password")
	@APIResponses(value = @APIResponse(responseCode = "200", description = "Success", content = @Content(mediaType = "application/json", schema = @Schema(implementation = User.class))))
	public UserDto loginUser(@Valid LoginDto loginDto) throws UserNotFoundException {
		return userService.loginUser(loginDto);
	}

	@POST
	@PermitAll
	// @RolesAllowed("ADMIN")
	@Operation(summary = "Adds a user", description = "Creates a user and persists into database")
	@APIResponses(value = @APIResponse(responseCode = "200", description = "Success", content = @Content(mediaType = "application/json", schema = @Schema(implementation = User.class))))
	public User createUser(@Valid UserDto userDto) {
		return userService.saveUser(userDto);
	}

	@PUT
	@RolesAllowed({ "USER", "ADMIN" })
	@Path("/{id}")
	@Operation(summary = "Updates a user", description = "Updates an existing user by id")
	@APIResponses(value = {
			@APIResponse(responseCode = "200", description = "Success", content = @Content(mediaType = "application/json", schema = @Schema(implementation = User.class))),
			@APIResponse(responseCode = "404", description = "User not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionHandler.ErrorResponseBody.class))) })
	public User updateUser(@PathParam("id") int id, @Valid UserDto userDto) throws UserNotFoundException {
		return userService.updateUser(id, userDto);
	}

	@DELETE
	@RolesAllowed({ "USER", "ADMIN" })
	@Path("/{id}")
	@Operation(summary = "Deletes a user", description = "Deletes a user by id")
	@APIResponses(value = { @APIResponse(responseCode = "204", description = "Success"),
			@APIResponse(responseCode = "404", description = "User not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionHandler.ErrorResponseBody.class))) })
	public Response deleteUser(@PathParam("id") int id) throws UserNotFoundException {
		userService.deleteUser(id);
		return Response.status(Response.Status.NO_CONTENT).build();
	}
}