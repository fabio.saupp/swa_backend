package org.license.mgmt.controller;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.license.mgmt.dto.CustomerDto;
import org.license.mgmt.entity.Customer;
import org.license.mgmt.exception.CustomerNotFoundException;
import org.license.mgmt.exception.handler.ExceptionHandler;
import org.license.mgmt.service.CustomerService;

@RequestScoped
@Path("/v1/customers")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@SecurityScheme(securitySchemeName = "Basic Auth", type = SecuritySchemeType.HTTP, scheme = "basic")
public class CustomerController {

	private final CustomerService customerService;

    @Inject
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }
    
    @GET
    @RolesAllowed({"USER", "ADMIN"})
    @Operation(summary = "Gets customers", description = "Lists all available customers")
    @APIResponses(value = @APIResponse(responseCode = "200", description = "Success",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Customer.class))))
    public List<Customer> getCustomers(@QueryParam("name") String name) {
        return customerService.getAllCustomers(name);
    }
    
    @GET
    @RolesAllowed({"USER", "ADMIN"})
    @Path("/{id}")
    @Operation(summary = "Gets a customer", description = "Retrieves a customer by id")
    @APIResponses(value = {
            @APIResponse(responseCode = "200", description = "Success",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Customer.class))),
            @APIResponse(responseCode = "404", description="Customer not found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionHandler.ErrorResponseBody.class)))
    })
    public Customer getCustomer(@PathParam("id") int id) throws CustomerNotFoundException {
        return customerService.getCustomerById(id);
    }
    
    
    @GET
    @RolesAllowed({"USER", "ADMIN"})
    @Path("/user/{userId}")
    @Operation(summary = "Gets a customer", description = "Retrieves a customer by id")
    @APIResponses(value = {
            @APIResponse(responseCode = "200", description = "Success",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Customer.class))),
            @APIResponse(responseCode = "404", description="Customer not found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionHandler.ErrorResponseBody.class)))
    })
    public Customer getAllCustomerByUserId(@PathParam("userId") int id) throws CustomerNotFoundException {
        return customerService.getAllCustomerByUserId(id);
    }

    @POST
    @RolesAllowed("ADMIN")
    @Operation(summary = "Adds a customer", description = "Creates a customer and persists into database")
    @APIResponses(value = @APIResponse(responseCode = "200", description = "Success",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Customer.class))))
    public Customer createCustomer(@Valid CustomerDto customerDto) {
        return customerService.saveCustomer(customerDto.toCustomer());
    }

    @PUT
    @RolesAllowed("ADMIN")
    @Path("/{id}")
    @Operation(summary = "Updates a customer", description = "Updates an existing customer by id")
    @APIResponses(value = {
            @APIResponse(responseCode = "200", description = "Success",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Customer.class))),
            @APIResponse(responseCode = "404", description="Customer not found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionHandler.ErrorResponseBody.class)))
    })
    public Customer updateCustomer(@PathParam("id") int id, @Valid CustomerDto customerDto) throws CustomerNotFoundException {
    	return customerService.updateCustomer(id, customerDto.toCustomer());
    }

    @DELETE
    @RolesAllowed("ADMIN")
    @Path("/{id}")
    @Operation(summary = "Deletes a customer", description = "Deletes a customer by id")
    @APIResponses(value = {
            @APIResponse(responseCode = "204", description = "Success"),
            @APIResponse(responseCode = "404", description="Customer not found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionHandler.ErrorResponseBody.class)))
    })
    public Response deleteCustomer(@PathParam("id") int id) throws CustomerNotFoundException {
    	customerService.deleteCustomer(id);
        return Response.status(Response.Status.NO_CONTENT).build();
    }
}
