package org.license.mgmt.dto;

import javax.validation.constraints.NotBlank;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

public class LoginDto {
	@NotBlank
    @Schema(title = "Username", required = true)
    private String username;

    @NotBlank
    @Schema(title = "Password", required = true)
    private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
