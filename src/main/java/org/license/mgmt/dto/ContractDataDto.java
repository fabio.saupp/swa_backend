package org.license.mgmt.dto;

import java.time.LocalDate;
import java.util.List;

import org.license.mgmt.entity.Customer;
import org.license.mgmt.entity.User;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ContractDataDto {
	
	private long id;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-mm-dd")
	private LocalDate startDate;

	private Customer customer;
	
	private List<User> users;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-mm-dd")
	private LocalDate endDate;
	
	private String ip1;
	
	private String ip2;
	
	private String ip3;
	
	private String version;
	
	private double feature1;
	
	private double feature2;
	
	private double feature3;
	
	private String licenseKey;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public String getIp1() {
		return ip1;
	}

	public void setIp1(String ip1) {
		this.ip1 = ip1;
	}

	public String getIp2() {
		return ip2;
	}

	public void setIp2(String ip2) {
		this.ip2 = ip2;
	}

	public String getIp3() {
		return ip3;
	}

	public void setIp3(String ip3) {
		this.ip3 = ip3;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public double getFeature1() {
		return feature1;
	}

	public void setFeature1(double feature1) {
		this.feature1 = feature1;
	}

	public double getFeature2() {
		return feature2;
	}

	public void setFeature2(double feature2) {
		this.feature2 = feature2;
	}

	public double getFeature3() {
		return feature3;
	}

	public void setFeature3(double feature3) {
		this.feature3 = feature3;
	}

	public String getLicenseKey() {
		return licenseKey;
	}

	public void setLicenseKey(String licenseKey) {
		this.licenseKey = licenseKey;
	}

	public ContractDataDto(long id, LocalDate startDate, Customer customer, List<User> users, LocalDate endDate,
			String ip1, String ip2, String ip3, String version, double feature1, double feature2, double feature3,
			String licenseKey) {
		super();
		this.id = id;
		this.startDate = startDate;
		this.customer = customer;
		this.users = users;
		this.endDate = endDate;
		this.ip1 = ip1;
		this.ip2 = ip2;
		this.ip3 = ip3;
		this.version = version;
		this.feature1 = feature1;
		this.feature2 = feature2;
		this.feature3 = feature3;
		this.licenseKey = licenseKey;
	}
	
	
	
}
