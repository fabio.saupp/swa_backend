package org.license.mgmt.dto;

import java.util.List;

import org.license.mgmt.entity.Contract;
import org.license.mgmt.entity.Customer;

public class CustomerDto {
	private String name;
	private String department;
	
	private String address;

	List<Contract> contracts;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<Contract> getContracts() {
		return contracts;
	}

	public void setContracts(List<Contract> contracts) {
		this.contracts = contracts;
	}
	
	public Customer toCustomer() {
		Customer customer = new Customer();
        customer.setAddress(address);
        customer.setDepartment(department);
        customer.setName(name);
        customer.setContracts(contracts);
        return customer;
    }
}
