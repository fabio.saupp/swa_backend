package org.license.mgmt.dto;

import javax.validation.constraints.NotBlank;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.license.mgmt.entity.User;

@Schema(name="UserDTO", description="User representation to create")
public class UserDto {
	
    private long id;

    @NotBlank
    @Schema(title = "Email", required = true)
    private String email;
    
    @Schema(title = "customerId", required = true)
    private long customerId;
    
    @NotBlank
    @Schema(title = "Username", required = true)
    private String username;

    @NotBlank
    @Schema(title = "Password", required = true)
    private String password;

    @Schema(title = "User role, either ADMIN or USER. USER is default")
    private String role;

    @NotBlank
    @Schema(title="User given name", required = true)
    private String firstName;

    @NotBlank
    @Schema(title="User surname", required = true)
    private String lastName;

//    @Min(value = 1, message = "The value must be more than 0")
//    @Max(value = 200, message = "The value must be less than 200")
//    @Schema(title="User age between 1 to 200", required = true)
//    private int age;
    
    @NotBlank
    @Schema(title="Phone", required = true)
    private String phone;
    
    @NotBlank
    @Schema(title="Mobile", required = true)
    private String mobile;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public User toUser() {
        User user = new User();
        user.setEmail(email);
        user.setUsername(username);
        user.setPassword(password);
        user.setRole(StringUtils.isBlank(role) ? "USER" : StringUtils.upperCase(role));
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMobile(mobile);
        user.setPhone(phone);
        return user;
    }
}