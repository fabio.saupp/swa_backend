package org.license.mgmt.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.license.mgmt.dto.LoginDto;
import org.license.mgmt.dto.UserDto;
import org.license.mgmt.entity.Customer;
import org.license.mgmt.entity.CustomerUserMap;
import org.license.mgmt.entity.User;
import org.license.mgmt.exception.UserNotFoundException;
import org.license.mgmt.repository.ContractUserMapRepository;
import org.license.mgmt.repository.CustomerRepository;
import org.license.mgmt.repository.CustomerUserMapRepository;
import org.license.mgmt.repository.UserRepository;

import io.quarkus.elytron.security.common.BcryptUtil;
import io.quarkus.panache.common.Parameters;

@ApplicationScoped
public class UserService {

	private final UserRepository userRepository;

	private final CustomerRepository customerRepository;

	private final CustomerUserMapRepository customerUserMapRepository;

	private final ContractUserMapRepository contractUserMapRepository;

	@Inject
	public UserService(UserRepository userRepository, CustomerUserMapRepository customerUserMapRepository,
			CustomerRepository customerRepository, ContractUserMapRepository contractUserMapRepository) {
		this.userRepository = userRepository;
		this.customerUserMapRepository = customerUserMapRepository;
		this.customerRepository = customerRepository;
		this.contractUserMapRepository = contractUserMapRepository;
	}

	public List<Customer> getAllCustomers() {
		return customerRepository.listAll();
	}

	public List<User> getAllUsersByCustomerId(long customerId) {
		return customerUserMapRepository.find("customer_id", customerId).list().stream()
				.map(each -> findById(each.getUserId())).collect(Collectors.toList());
	}

	public User getUserById(long id) throws UserNotFoundException {
		return userRepository.findByIdOptional(id)
				.orElseThrow(() -> new UserNotFoundException("There user doesn't exist"));
	}

	public UserDto getUserDtoById(long id) throws UserNotFoundException {
		UserDto dto = new UserDto();
		Optional<CustomerUserMap> optional = customerUserMapRepository.find("userId", id).firstResultOptional();
		if (optional.isPresent()) {
			dto.setCustomerId(optional.get().getCustomerId());
		}
		User user = userRepository.findByIdOptional(id)
				.orElseThrow(() -> new UserNotFoundException("There user doesn't exist"));
		dto.setFirstName(user.getFirstName());
		dto.setLastName(user.getLastName());
		dto.setEmail(user.getEmail());
		dto.setUsername(user.getUsername());
		dto.setPassword(user.getPassword());
		dto.setPhone(user.getPhone());
		dto.setMobile(user.getMobile());
		dto.setId(user.getId());
		dto.setRole(user.getRole());
		return dto;
	}

	public User findById(long id) {
		Optional<User> findByIdOptional = userRepository.findByIdOptional(id);
		if (findByIdOptional.isPresent()) {
			return findByIdOptional.get();
		} else {
			return null;
		}
	}

	public List<User> getAllUsers() {
		return userRepository.listAll();
	}

	@Transactional
	public User updateUser(long id, UserDto user) throws UserNotFoundException {

		Optional<CustomerUserMap> optional = customerUserMapRepository.find("userId", id).firstResultOptional();
		if (optional.isPresent()) {
			CustomerUserMap customerUserMap = optional.get();
			customerUserMap.setCustomerId(user.getCustomerId());
			customerUserMapRepository.persist(customerUserMap);
		} else {
			CustomerUserMap customerUserMap = new CustomerUserMap();
			customerUserMap.setUserId(id);
			customerUserMap.setCustomerId(user.getCustomerId());
			customerUserMapRepository.persist(customerUserMap);
		}
		User existingUser = getUserById(id);
		existingUser.setUsername(user.getUsername());
		if (!StringUtils.equals(user.getPassword(), existingUser.getPassword())
				&& !BcryptUtil.matches(user.getPassword(), existingUser.getPassword())) {
			existingUser.setPassword(user.getPassword());
		}
		existingUser.setFirstName(user.getFirstName());
		existingUser.setLastName(user.getLastName());
		existingUser.setMobile(user.getMobile());
		existingUser.setEmail(user.getEmail());
		existingUser.setPhone(user.getPhone());
		existingUser.setRole(user.getRole());
		userRepository.persist(existingUser);
		return existingUser;
	}

	@Transactional
	public User saveUser(UserDto userDto) {
		User user = userDto.toUser();
		userRepository.persistAndFlush(user);

		Optional<Customer> resultOptional = customerRepository.findByIdOptional(userDto.getCustomerId());
		if (resultOptional.isPresent()) {
			CustomerUserMap customerUserMap = new CustomerUserMap();
			customerUserMap.setCustomerId(resultOptional.get().getId());
			customerUserMap.setUserId(user.getId());
			customerUserMapRepository.persistAndFlush(customerUserMap);
		} else {
		}

		return user;
	}

	@Transactional
	public void deleteUser(long id) throws UserNotFoundException {
		customerUserMapRepository.delete("userId", id);
		contractUserMapRepository.delete("userId", id);
		userRepository.delete(getUserById(id));
	}

	public User findByUserName(String username) {
		return userRepository.find("username", username).singleResult();
	}

	public UserDto loginUser(@Valid LoginDto loginDto) throws UserNotFoundException {
		Parameters param = new Parameters();
		param.and("username", loginDto.getUsername());
		Optional<User> user = userRepository.find("username", loginDto.getUsername()).singleResultOptional();
		if (user.isPresent() && BcryptUtil.matches(loginDto.getPassword(), user.get().getPassword())) {
			User loggedInUser = user.get();
			UserDto dto = new UserDto();
			dto.setId(loggedInUser.getId());
			dto.setEmail(loggedInUser.getEmail());
			dto.setUsername(loggedInUser.getUsername());
			dto.setPassword(loginDto.getPassword());
			dto.setRole(loggedInUser.getRole());
			dto.setFirstName(loggedInUser.getFirstName());
			dto.setLastName(loggedInUser.getLastName());
			dto.setMobile(loggedInUser.getMobile());
			dto.setPhone(loggedInUser.getPhone());
			return dto;
		} else {
			throw new UserNotFoundException("Incorrect username or password!");
		}
	}

}