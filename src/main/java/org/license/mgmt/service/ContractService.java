package org.license.mgmt.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.license.mgmt.dto.ContractDataDto;
import org.license.mgmt.dto.ContractDto;
import org.license.mgmt.entity.Contract;
import org.license.mgmt.entity.ContractUserMap;
import org.license.mgmt.entity.Customer;
import org.license.mgmt.entity.User;
import org.license.mgmt.exception.ContractNotFoundException;
import org.license.mgmt.exception.CustomerNotFoundException;
import org.license.mgmt.exception.UserNotFoundException;
import org.license.mgmt.repository.ContractUserMapRepository;
import org.license.mgmt.repository.ContractRepository;

@ApplicationScoped
public class ContractService {

	private final ContractRepository contractRepository;

	private final UserService userService;

	private final ContractUserMapRepository contractUserMapRepository;

	private final CustomerService customerService;

	@Inject
	public ContractService(ContractRepository contractRepository, ContractUserMapRepository contractUserMapRepository,
			UserService userService, CustomerService customerService) {
		this.contractRepository = contractRepository;
		this.contractUserMapRepository = contractUserMapRepository;
		this.userService = userService;
		this.customerService = customerService;
	}

	public Contract getContractById(long id) throws ContractNotFoundException {
		return contractRepository.findByIdOptional(id)
				.orElseThrow(() -> new ContractNotFoundException("There contract doesn't exist"));
	}

	public Contract getContractByIdExist(long id) {
		return contractRepository.findById(id);
	}

	public List<ContractDataDto> getAllContracts() {
		List<Contract> listAll = contractRepository.listAll();

		return listAll.stream()
				.map(each -> new ContractDataDto(each.getId(), each.getStartDate(), each.getCustomer(),
						getUserContractMapCount(each.getId()), each.getEndDate(), each.getIp1(), each.getIp2(),
						each.getIp3(), each.getVersion(), each.getFeature1(), each.getFeature2(), each.getFeature3(),
						each.getLicenseKey()))
				.collect(Collectors.toList());

		// return contractRepository.listAll();
	}

	public List<Contract> getAllContractsByCustomerId(long customerId) {
		return contractRepository.find("CUSTOMER_ID", customerId).list();
	}

	public List<Contract> getAllContractsByUserId(long userId) {
		return contractUserMapRepository.find("userId", userId).list().stream()
				.map(each -> getContractByIdExist(each.getContractId())).collect(Collectors.toList());
	}

	@Transactional
	public Contract updateContract(long id, ContractDto contractDto) throws ContractNotFoundException {
		Customer customer = null;
		try {
			customer = customerService.getCustomerById(contractDto.getCustomerId());
		} catch (CustomerNotFoundException e) {
			e.printStackTrace();
		}
		Contract contract = contractDto.toContract();
		List<String> users = contractDto.getUsers();
		List<User> userList = users.stream().map(each -> userService.findByUserName(each)).collect(Collectors.toList());
		Contract existingContract = getContractById(id);
		existingContract.setStartDate(contract.getStartDate());
		existingContract.setEndDate(contract.getEndDate());
		existingContract.setFeature1(contract.getFeature1());
		existingContract.setFeature2(contract.getFeature2());
		existingContract.setFeature3(contract.getFeature3());
		existingContract.setIp1(contract.getIp1());
		existingContract.setIp2(contract.getIp2());
		existingContract.setIp3(contract.getIp3());
		existingContract.setLicenseKey(contract.getLicenseKey());
		existingContract.setVersion(contract.getVersion());
		existingContract.setCustomer(customer);
		// existingContract.set

		deleteOldUserContractMap(id);
		userList.stream().forEach(each -> {
			try {
				saveContractUserMap(existingContract, each);
			} catch (UserNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		contractRepository.persist(existingContract);
		return existingContract;
	}

	@Transactional
	public Contract saveContract(Contract contract) {
		try {
			contractRepository.persistAndFlush(contract);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return contract;
	}

	@Transactional
	public void saveContractUserMap(Contract contract, User user) throws UserNotFoundException {
		List<ContractUserMap> list = contractUserMapRepository.find("userId", user.getId()).list();
		if (list.size() <= 2) {
			boolean isAlreadyMapped = false;
			for (ContractUserMap contractUserMap : list) {
				if (contractUserMap.getContractId() == contract.getId()) {
					isAlreadyMapped = true;
				}
			}
			if (!isAlreadyMapped) {
				ContractUserMap map = new ContractUserMap();
				map.setContractId(contract.getId());
				map.setUserId(user.getId());
				contractUserMapRepository.persistAndFlush(map);
			}
		} else {
			throw new UserNotFoundException("User already mapped to contracts : " + list.size());
		}
	}

	public List<User> getUserContractMapCount(long contractId) {
		List<ContractUserMap> list = contractUserMapRepository.find("contract_id", contractId).list();
		return list.stream().map(each -> userService.findById(each.getUserId())).collect(Collectors.toList());
	}

	public void deleteOldUserContractMap(long contractId) {
		contractUserMapRepository.delete("contract_id", contractId);
	}

	@Transactional
	public void deleteContract(long id) throws ContractNotFoundException {
		Contract contract = getContractById(id);
		contractUserMapRepository.delete("contract_id", id);
		contractRepository.delete(contract);
	}
}
