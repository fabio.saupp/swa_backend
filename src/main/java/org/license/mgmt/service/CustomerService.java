package org.license.mgmt.service;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.license.mgmt.entity.Customer;
import org.license.mgmt.entity.CustomerUserMap;
import org.license.mgmt.exception.CustomerNotFoundException;
import org.license.mgmt.repository.CustomerRepository;
import org.license.mgmt.repository.CustomerUserMapRepository;

@ApplicationScoped
public class CustomerService {

	private final CustomerRepository customerRepository;

	private final CustomerUserMapRepository customerUserMapRepository;

	@Inject
	public CustomerService(CustomerRepository customerRepository, CustomerUserMapRepository customerUserMapRepository) {
		this.customerRepository = customerRepository;
		this.customerUserMapRepository = customerUserMapRepository;
	}

	public Customer getCustomerById(long id) throws CustomerNotFoundException {
		return customerRepository.findByIdOptional(id)
				.orElseThrow(() -> new CustomerNotFoundException("There customer doesn't exist"));
	}

	public Customer getAllCustomerByUserId(long userId) throws CustomerNotFoundException {
		CustomerUserMap customerUserMap = customerUserMapRepository.find("userId", userId).firstResult();
		return customerRepository.findByIdOptional(customerUserMap.getCustomerId())
				.orElseThrow(() -> new CustomerNotFoundException("There customer doesn't exist"));
	}

	public List<Customer> getAllCustomers(String name) {
		if (name == null || (name != null & name.trim().equals(""))) {
			return customerRepository.listAll();
		} else {
			// String searchInput = "%" + name + "%";
			String searchInput = name + "%";
			return customerRepository.find("name like ?1", searchInput).list();
		}

	}

	@Transactional
	public Customer updateCustomer(long id, Customer customer) throws CustomerNotFoundException {
		Customer existingCustomer = getCustomerById(id);
		existingCustomer.setAddress(customer.getAddress());
		existingCustomer.setDepartment(customer.getDepartment());
		existingCustomer.setName(customer.getName());
		if (customer.getContracts() != null && !customer.getContracts().isEmpty()) {
			existingCustomer.setContracts(customer.getContracts());
		}
		customerRepository.persistAndFlush(existingCustomer);
		return existingCustomer;
	}

	@Transactional
	public Customer saveCustomer(Customer customer) {
		customerRepository.persistAndFlush(customer);
		return customer;
	}

	@Transactional
	public void deleteCustomer(long id) throws CustomerNotFoundException {
		customerRepository.delete(getCustomerById(id));
	}
}
